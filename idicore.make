api = 2
core = 7.31
projects[drupal] = "7.31"

;themes
projects[omega] = "2.0-alpha10"


; default modules
projects[admin][subdir] = "contrib"
projects[admin][version] = "2.0-beta3"

projects[admin_tools][subdir] = "contrib"
projects[admin_tools][version] = "1.0-beta1"

projects[views][subdir] = "contrib"
projects[views][version] = "3.0-alpha1"

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.0-alpha2"

projects[context][subdir] = "contrib"
projects[context][version] = "3.0-alpha3"

projects[globalredirect][subdir] = "contrib"
projects[globalredirect][version] = "1.3"

; leaving out block_class until an official release is made
;projects[block_class][subdir] = "contrib"
;projects[block_class][version] = "1.x-dev"

projects[extlink][subdir] = "contrib"
projects[extlink][version] = "1.12"

projects[token][subdir] = "contrib"
projects[token][version] = "1.0-beta1"

projects[mollom][subdir] = "contrib"
projects[mollom][version] = "1.0"

projects[pathauto][subdir] = "contrib"
projects[pathauto][version] = "1.0-beta1"

projects[menu_expanded][subdir] = "contrib"
projects[menu_expanded][version] = "1.0-beta1"

projects[client_ui_control][subdir] = "contrib"
projects[client_ui_control][version] = "1.0-alpha3"

projects[omega_tools][subdir] = "contrib"
projects[omega_tools][version] = "2.0-beta4"

projects[delta][subdir] = "contrib"
projects[delta][version] = "2.0-alpha2"

projects[google_analytics][subdir] = "contrib"
projects[google_analytics][version] = "1.0"

; development modules
projects[devel][subdir] = "development"
projects[devel][version] = "1.0"

projects[coder][subdir] = "development"
projects[coder][version] = "1.0-beta6"

projects[environment_indicator][subdir] = "development"
projects[environment_indicator][version] = "1.0-alpha1"